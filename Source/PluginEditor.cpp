/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
DelayAudioProcessorEditor::DelayAudioProcessorEditor (DelayAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (600, 400);
    
    gain_dry_slider.setRange(0, 1);
    gain_dry_slider.addListener(this);
    gain_dry_label.setText(String("dry"), dontSendNotification);
    gain_dry_label.attachToComponent(&gain_dry_slider, true);
    
    gain_wet_slider.setRange(0, 1);
    gain_wet_slider.addListener(this);
    gain_wet_label.setText(String("wet"), dontSendNotification);
    gain_wet_label.attachToComponent(&gain_wet_slider, true);
    
    delay_time_slider.setRange(0.0, processor.max_delay_time);
    delay_time_slider.addListener(this);
    delay_time_label.setText(String("delay time"), dontSendNotification);
    delay_time_label.attachToComponent(&delay_time_slider, true);
}

DelayAudioProcessorEditor::~DelayAudioProcessorEditor()
{
}

//==============================================================================
void DelayAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
//
//    g.setColour (Colours::white);
//    g.setFont (15.0f);
//    g.drawFittedText (String("dry ") + String(processor.gain_dry), 300, 100, 100, 30, Justification::centred, 1);
//    g.drawFittedText (String("wet ") + String(processor.gain_wet), 300, 140, 100, 30, Justification::centred, 1);
}

void DelayAudioProcessorEditor::resized()
{
    gain_dry_slider.setBounds(100, 100, 200, 20);
    addAndMakeVisible(gain_dry_slider);
    addAndMakeVisible(gain_dry_label);
    
    gain_wet_slider.setBounds(100, 130, 200, 20);
    addAndMakeVisible(gain_wet_slider);
    addAndMakeVisible(gain_wet_label);
    
    delay_time_slider.setBounds(100, 160, 200, 20);
    addAndMakeVisible(delay_time_slider);
    addAndMakeVisible(delay_time_label);
}

void DelayAudioProcessorEditor::sliderValueChanged (Slider *slider)
{
    repaint();
    
    if (slider == &(gain_dry_slider))
    {
        processor.gain_dry = slider->getValue();
        std::cout << "dry: " << processor.gain_dry << std::endl;
    }
    
    if (slider == &(gain_wet_slider))
    {
        processor.gain_wet = slider->getValue();
        std::cout << "wet: " << processor.gain_wet << std::endl;
    }
    
    if (slider == &(delay_time_slider)) {
        processor.delay_time = slider->getValue();
        std::cout << "delay time: " << processor.delay_time << std::endl;
    }
}
