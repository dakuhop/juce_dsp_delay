/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <math.h>


//==============================================================================
DelayAudioProcessor::DelayAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    // just some initial values; will be overwritten by editor soon
    // the rest of the setup moved to "prepareToPlay()"
    max_delay_time = 3;
    delay_time = 1;
}

DelayAudioProcessor::~DelayAudioProcessor()
{
}

//==============================================================================
const String DelayAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool DelayAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool DelayAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool DelayAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double DelayAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int DelayAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int DelayAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DelayAudioProcessor::setCurrentProgram (int index)
{
}

const String DelayAudioProcessor::getProgramName (int index)
{
    return {};
}

void DelayAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void DelayAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    max_delay_samples = max_delay_time * sampleRate;
    int numChannels = getTotalNumInputChannels();
    delay_array = new float*[numChannels]; // we need a two-dimensional array for the channels
    counter = new int[numChannels]; // also two counters now for each channel
    current_delay_in_samples = new float[numChannels];
    delay_in_samples = delay_time * sampleRate;
    g = 0;
    for (int channel = 0; channel < numChannels; channel++) {
        delay_array[channel] = new float[max_delay_samples]; // set array into array
        counter[channel] = 0; // set the two counters to 0
        for (int sample_index = 0; sample_index < max_delay_samples; sample_index++) delay_array[channel][sample_index] = 0; // write all init samples to 0
        current_delay_in_samples[channel] = delay_in_samples;
    }
    this->sampleRate = sampleRate; // so that we can access it in editor
}

void DelayAudioProcessor::releaseResources()
{
   
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool DelayAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void DelayAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();
    
    float* offsetted_counter = new float[totalNumInputChannels]; // we also need two offsetted counters
    this->sampleRate = getSampleRate(); // so that we can access it in editor; "getSampleRate()" only works in here.. dunno why
    delay_in_samples = delay_time * sampleRate; // update delay-time
//    float current_delay_in_samples = delay_in_samples;
//    std::cout << current_delay_in_samples[0] << std::endl;
//    std::cout << current_delay_in_samples[1] << std::endl;

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage)float
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        float* channelData = buffer.getWritePointer (channel);
        
        for (int i = 0; i < buffer.getNumSamples(); i++) {
            current_delay_in_samples[channel] = current_delay_in_samples[channel] + g;
            offsetted_counter[channel] = fmod((counter[channel] + (max_delay_samples - current_delay_in_samples[channel])),
                                              max_delay_samples);
            
            if ((int)current_delay_in_samples[channel] == delay_in_samples) {
                g = 0;
            } else if ((int)current_delay_in_samples[channel] < delay_in_samples) {
                g = g_probe * 1;
            } else {
                g = g_probe * -1;
            }
            
//            offsetted_counter = offsetted_counter[channel] - g;
            
            long rpi = (long)floor(offsetted_counter[channel]);
            float a = offsetted_counter[channel] - (float)rpi;
//            y = a * A[rpi] + (1-a) * A[rpi+1];
            float delay_sample = (a * delay_array[channel][rpi]) + ((1-a) * delay_array[channel][rpi + 1]);
//            rptr += 1;
//            offsetted_counter[channel] = offsetted_counter[channel] - g;
            
             // with this variable we can have 'delay_time' <= 'max_delay_time' not as previously '<'
            
            delay_array[channel][counter[channel]] = channelData[i];
            
            channelData[i] = (channelData[i] * gain_dry) + (delay_sample * gain_wet);
            
            counter[channel] = (counter[channel] + 1) % max_delay_samples;
        }
    }
}

//==============================================================================
bool DelayAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* DelayAudioProcessor::createEditor()
{
    return new DelayAudioProcessorEditor (*this);
}

//==============================================================================
void DelayAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void DelayAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new DelayAudioProcessor();
}
